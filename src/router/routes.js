
const routes = [
  {
    path: '/',
    component: () => import('layouts/Main.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/require1',
    component: () => import('layouts/Require1.vue')
  },
  {
    path: '/map',
    component: () => import('layouts/Map.js.vue')
  },
  {
    path: '/require2',
    component: () => import('layouts/Require2.vue')
  },
  {
    path: '/require3',
    component: () => import('layouts/Require3.vue')
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
