/*
export function someGetter (state) {
}
*/
export function GET_LOADING (state) {
  return state.loading
}
export function GET_USER (state) {
  return state.user
}
export function GET_ERROR (state) {
  return state.error
}
export function GET_IMAGES (state) {
  return state.images
}
export function GET_PROVINCES (state) {
  return state.provinces
}
export function GET_DISTRICTS (state) {
  return state.districts
}
export function GET_WARDS (state) {
  return state.wards
}
