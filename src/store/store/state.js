export default function () {
  return {
    loading: false,
    user: {},
    error: '',
    images: [],
    provinces: [],
    districts: [],
    wards: []
  }
}
