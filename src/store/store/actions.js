/*
export function someAction (context) {
}
*/
import axios from 'axios'
// eslint-disable-next-line camelcase
// import jwt_decode from 'jwt-decode'

export function onLogin ({ commit }, data) {
  return axios.post('https://test-capi.poicollector.com/api/user/login', {
    username: data.name,
    password: data.password
  })
    .then(result => {
      if (result.data.data) {
        axios.defaults.headers.common['Token-Key'] = result.data.data.token
        localStorage.setItem('token', result.data.data.token)
        commit('SET_USER', {
          token: result.data.data,
          name: data.name,
          password: data.password
        })
      } else {
        commit('SET_ERROR', result.data.result.message)
      }
    })
    .catch(error => {
      commit('SET_ERROR', error.code)
    })
}
export function setLOADING ({ commit }) {
  commit('SET_LOADING')
}
export function setDONELOADING ({ commit }) {
  commit('SET_DONE_LOADING')
}
export function setError ({ commit }) {
  commit('SET_ERROR', '')
}
export function fetchData ({ commit }) {
  if (localStorage.getItem('token')) {
    axios.defaults.headers.common['Token-Key'] = localStorage.getItem('token')
    return axios.get('https://test-capi.poicollector.com/api/poi/assigned/v2')
      .then(result => {
        if (result.data.result.code === 0) {
          commit('SET_ERROR', '')
          var images = []
          result.data.data.data.forEach(image => {
            images.push(image)
          })
          commit('FETCH_IMAGE', images)
        }
      })
      .catch(error => {
        console.log(error)
      })
  } else {
    window.alert('No token was save!')
  }
}
export function fetchProvince ({ commit }) {
  return axios.get('https://test-capi.poicollector.com/api/define/province')
    .then(result => {
      if (result.data) {
        commit('SET_PROVINCE', result.data)
      } else {
        console.log('No province found!')
      }
    })
    .catch(error => {
      console.log(error)
    })
}
export function fetchDistrict ({ commit }, id) {
  return axios.get(`https://test-capi.poicollector.com/api/define/district?province_id=${id}`)
    .then(result => {
      if (result.data) {
        commit('SET_DISTRICTS', result.data.data.data)
      } else {
        console.log('No district found!')
      }
    })
    .catch(error => {
      console.log(error)
    })
}
export function fetchWard ({ commit }, id) {
  return axios.get(`https://test-capi.poicollector.com/api/define/ward?district_id=${id}`)
    .then(result => {
      if (result.data) {
        commit('SET_WARDS', result.data.data.data)
      } else {
        console.log('No ward found!')
      }
    })
    .catch(error => {
      console.log(error)
    })
}
