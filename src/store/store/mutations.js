/*
export function someMutation (state) {
}
*/
export function SET_LOADING (state) {
  state.loading = true
}

export function SET_DONE_LOADING (state) {
  state.loading = false
}
export function SET_USER (state, data) {
  state.user = {
    token: data.token.token,
    name: data.name,
    password: data.password
  }
  console.log(state.user)
}
export function SET_ERROR (state, data) {
  state.error = data
}
export function FETCH_IMAGE (state, data) {
  state.images = data
}
export function SET_PROVINCE (state, data) {
  state.provinces = data.data.data
}
export function SET_DISTRICTS (state, data) {
  state.districts = data
}
export function SET_WARDS (state, data) {
  state.wards = data
}
